# Core-Validator

## ErrorSchema
Use core_validator.ErrorSchema if you don't know which format you're gonna use.

## Quick Start

### DTO
```python
class CommentDto(BaseModel):
    comment: str
    post_id: int
    owner_id: int
```


### Add error source (error context)
```python
class Source(BaseModel):
    local: str
```

### DTO Validator
```python
@dataclasses.dataclass()
class CommentValidator(Validator[CommentDto, ErrorSchema[Source]]):
    @validate
    async def test1(self):
        post_ids = list(range(1, 10))
        if self.data.post_id not in post_ids:
            self.context.add_error(
                ErrorSchema(
                    code=ErrorCodeEnum.not_found.value,
                    message="Id doen't not exists",
                    detail=f"Post with id={self.data.post_id} not found",
                    source=Source(
                        local="data/post_id",
                    ),
                )
            )

    @validate
    @pre_state(lambda self: self.data.owner_id > 0, negative_id_error)
    async def test2(self):
        owner_ids = list(range(1, 10))

        if self.data.owner_id not in owner_ids:
            self.context.add_error(
                ErrorSchema(
                    code=ErrorCodeEnum.not_found.value,
                    message="Id doen't not exists",
                    detail=f"User with id={self.data.post_id} not found",
                    source=Source(
                        local="data/owner_id",
                    ),
                )
            )
```

### Use
```python
async def function():
    validator = CommentValidator()
    errors = await validator.errors(comment)
    # or
    await validator.validate() # raise ValidationError

```
